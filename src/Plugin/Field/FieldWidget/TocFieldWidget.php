<?php

declare(strict_types=1);

namespace Drupal\toc_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\toc_api\Entity\TocType;

/**
 * Plugin implementation of the 'toc_field' widget.
 *
 * @FieldWidget(
 *   id = "toc_field_widget",
 *   module = "toc_field",
 *   label = @Translation("Toc Field Widget"),
 *   field_types = {
 *     "toc_field"
 *   }
 * )
 */
class TocFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';

    $toc_types = TocType::loadMultiple();
    $options = [];
    foreach ($toc_types as $toc_type) {
      $options[$toc_type->id()] = $toc_type->label();
    }
    \ksort($options);

    $element += [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $value,
      '#empty_option' => $this->t("- None -"),
    ];

    return ['value' => $element];
  }

}
