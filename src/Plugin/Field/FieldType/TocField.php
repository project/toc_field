<?php

declare(strict_types=1);

namespace Drupal\toc_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a TOC field type.
 *
 * @FieldType(
 *   id = "toc_field",
 *   label = @Translation("Table of Contents"),
 *   module = "field_toc",
 *   default_formatter = "toc_field_formatter",
 *   default_widget = "toc_field_widget",
 * )
 */
class TocField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => '128',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('TOC Type')->__toString());

    return $properties;
  }

}
