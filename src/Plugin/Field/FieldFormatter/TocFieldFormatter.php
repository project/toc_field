<?php

declare(strict_types=1);

namespace Drupal\toc_field\Plugin\Field\FieldFormatter;

// Boilerplate for Dependency Injection.
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

// Other.
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\toc_api\Entity\TocType;
use Drupal\toc_api\TocBuilder;
use Drupal\toc_api\TocManager;
use Drupal\toc_field\TocFieldHelper;

/**
 * Plugin implementation of the 'TOC Field' formatter.
 *
 * @FieldFormatter(
 *   id = "toc_field_formatter",
 *   label = @Translation("Table of Contents"),
 *   field_types = {
 *     "toc_field"
 *   }
 * )
 */
class TocFieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  // Boilerplate.

  /**
   * Drupal ConfigFactory service container.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  // Injected services.

  /**
   * The TOC Field Helper.
   *
   * @var \Drupal\toc_field\TocFieldHelper
   */
  protected $tocFieldHelper;

  /**
   * The TOC API Manager.
   *
   * @var \Drupal\toc_api\TocManager
   */
  protected $tocManager;

  /**
   * The TOC API Builder.
   *
   * @var \Drupal\toc_api\TocBuilder
   */
  protected $tocBuilder;

  /**
   * The Drupal Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs the TocFieldFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\toc_field\TocFieldHelper $toc_field_helper
   *   The TOC Field Helper utility service.
   * @param \Drupal\toc_api\TocManager $toc_manager
   *   The TOC API Manager service.
   * @param \Drupal\toc_api\TocBuilder $toc_builder
   *   The TOC Builder service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The Renderer service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   */
  public function __construct(
    $plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, string $label, string $view_mode, array $third_party_settings,  // Boilerplate.
    TocFieldHelper $toc_field_helper, TocManager $toc_manager, TocBuilder $toc_builder, RendererInterface $renderer, ConfigFactory $config_factory // Injected services.
  ) {
    // Boilerplate.
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->configFactory = $config_factory;

    // Injected services.
    $this->tocFieldHelper = $toc_field_helper;
    $this->tocManager = $toc_manager;
    $this->tocBuilder = $toc_builder;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      // Boilerplate.
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],

      // Injected services.
      $container->get('toc_field.helper'),
      $container->get('toc_api.manager'),
      $container->get('toc_api.builder'),
      $container->get('renderer'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = $this->t(
      'Displays the Table of Contents. Source field: @field', [
        '@field' => $this->getSetting('toc_source_field') ?? $this->t('none'),
      ])->__toString();

    return [$summary];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'toc_source_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $entity_type = $form['#entity_type'];
    $bundle = $form['#bundle'];
    $options = $this->tocFieldHelper
      ->getSourceFieldCandidates($entity_type, $bundle);

    $desc = $this->t(
      'Select a text field (long, formatted) as source field for the TOC.'
    );

    $form['toc_source_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Field'),
      '#description' => $desc,
      '#options' => $options,
      '#default_value' => $this->getSetting('toc_source_field'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    $element['#theme'] = 'toc_field';

    foreach ($items as $delta => $item) {
      $entity = $item->getEntity();
      $tocTypeId = $item->value;
      $toc = $this->renderToc($entity, $tocTypeId);

      $element['#toc'] = [
        '#markup' => Markup::create($toc),
      ];
    }

    return $element;
  }

  /**
   * Builds and renders a TOC based on the entity's TOC source field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity whose TOC source field to base the TOC upon.
   * @param string $tocTypeId
   *   The type of TOC to render (see /admin/structure/toc).
   *
   * @return string
   *   The rendered TOC.
   */
  private function renderToc(ContentEntityInterface $entity, string $tocTypeId): string {

    $toc_field_name = $this->tocFieldHelper
      ->getTocSourceFieldName($entity, $this->viewMode);

    if ($entity->hasField($toc_field_name) === FALSE) {
      return '';
    }

    // Get the completely rendered source field value.
    $source_field_content_raw = $entity->get($toc_field_name)->getValue();
    $source_field_content = $source_field_content_raw[0]['value'];

    // Get the options for the requested TOC type.
    /** @var \Drupal\toc_api\TocTypeInterface $toc_type */
    $toc_type = TocType::load($tocTypeId);

    $options = (!is_null($toc_type)) ? $toc_type->getOptions() : [];

    // Create a TOC instance using the TOC manager.

    /** @var \Drupal\toc_api\TocInterface $toc */
    $toc = $this->tocManager
      ->create('toc_filter', $source_field_content, $options);

    // Build TOC.
    $toc_render_array = $this->tocBuilder->buildToc($toc);

    // Render TOC.
    $rendered_toc = (string) $this->renderer->render($toc_render_array);
    return $rendered_toc;
  }

}
