<?php

declare(strict_types=1);

namespace Drupal\toc_field;

use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides the TocFieldHelper class.
 */
class TocFieldHelper {

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Entity Field Manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EntityFieldManagerInterface $entityFieldManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Returns the name of the TOC Field on the given entity, if any.
   *
   * Assumption: only one TOC Field will be present on any given entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The name of the TOC Field.
   */
  public function getTocFieldName(ContentEntityInterface $entity): string {
    $toc_field_name = '';
    foreach ($entity->getFieldDefinitions() as $field) {
      /** @var  \Drupal\Core\Field\FieldDefinitionInterface $field */
      if ($field->getType() == 'toc_field') {
        $toc_field_name = $field->getName();
        continue;
      }
    }
    return $toc_field_name;
  }

  /**
   * Returns the field to use as the TOC field's source field.
   *
   * This is set in the entity's TOC field's display settings
   * ('Manage Display').
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity for which the TOC will be generated.
   * @param string $view_mode
   *   The view mode for which the TOC will be generated.
   *
   * @return string
   *   The name of the provided entity's TOC source field.
   */
  public function getTocSourceFieldName(ContentEntityInterface $entity, string $view_mode): string {

    $entity_type = $entity->getEntityType()->id();
    $bundle = $entity->bundle();
    $toc_field_name = $this->getTocFieldName($entity);

    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
    $view_display = $this->entityTypeManager
      ->getStorage('entity_view_display')
      ->load("{$entity_type}.{$bundle}.{$view_mode}");

    if (is_null($view_display)) {
      // Fall back to the 'default' view mode.
      $view_display = $this->entityTypeManager
        ->getStorage('entity_view_display')
        ->load("{$entity_type}.{$bundle}.default");
    }

    /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
    $display_renderer = $view_display
      ->getRenderer($toc_field_name);
    
    if (is_null($display_renderer)) {
      return '';
    }
    
    $display_settings = $display_renderer->getSettings();

    if (!isset($display_settings['toc_source_field'])) {
      return '';
    }

    return $display_settings['toc_source_field'];
  }

  /**
   * Returns suitable source fields on the provided entity type and bundle.
   *
   * A TOC is generated from a large text field that allows H2-H6 headers.
   * As such, only field types that can contain this markup are suitable
   * source field candidates.
   *
   * @param string $entity_type
   *   The entity type to inspect.
   * @param string $bundle
   *   The bundle to inspect.
   *
   * @return array
   *   Returns an array in the form ['field_name' => 'field_name'].
   */
  public function getSourceFieldCandidates(string $entity_type, string $bundle): array {

    // Retrieve all the fields on the entity bundle.
    $field_definitions = $this->entityFieldManager
      ->getFieldDefinitions($entity_type, $bundle);

    // The list of field types suitable to be a source for a TOC.
    $candidate_field_types = [
      'text_long',
      'text_with_summary',
    ];

    $candidate_field_names = [];

    // Loop through all fields and keep fields that match the candidate types.
    foreach ($field_definitions as $field_definition) {
      // Skip BaseFields; we're only interested in (manually) added fields.
      if (!$field_definition instanceof FieldConfig) {
        continue;
      }

      /** @var \Drupal\field\Entity\FieldConfig $field_definition */
      $field_type_name = $field_definition->getType();
      if (in_array($field_type_name, $candidate_field_types)) {
        $field_name = $field_definition->getName();
        $candidate_field_names[$field_name] = $field_name;
      }
    }

    return $candidate_field_names;
  }

}
