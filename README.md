# TOC Field

## Table of Contents

* Introduction
* Usage
* Managing TOC Types
* Theming the TOC
* Alternatives
* Credits

## Introduction

This module provides a 'TOC Field' field type that lets you display 
an automatically generated Table of Contents (TOC) derived from a 
long text field.

The most common use case is to add this field to a node and configure it 
to use the _body_ field as source field.

That said, you can add this field to any fieldable content entity type 
(such as nodes), and select any long, formatted text field as the 
source field from which to derive the TOC.

## Usage

1. Install the module.

2. Add a field of the type _Table of Contents_ to your content type (or other content entity types).

3. In _Manage display_, configure the field and select which source field to use.

4. On each node that should display a TOC, select one of the predefined _TOC Types_ or choose '- none -'.

## Managing TOC Types 

The [toc_api](https://drupal.org/project/toc_api) module, on which this module 
depends, is responsible for all of the TOC functionality.

Configuring TOC Types is provided by the _toc_api_ module at 
admin/structure/toc.

## Theming the TOC

The provided `templates/toc-field.html.twig` template only exists to let you 
wrap the field's output in whichever markup you want.

You can theme the TOC itself by overriding the templates provided by the 
toc_api module.

## Alternatives

If you're looking for something that lets you embed a `[toc]` token 
have a look at the [toc_filter](https://drupal.org/project/toc_filter) module.

## Credits

* Original creator and maintainer: [jpoesen](https://www.drupal.org/u/jpoesen)
* The TOC API project: [https://drupal.org/project/toc_api](https://drupal.org/project/toc_api)
* Development sponsored by:
 * [TrainingCloud.io](https://trainingcloud.io/)
 * [London Borough of Hammersmith & Fulham](https://lbhf.gov.uk/)
